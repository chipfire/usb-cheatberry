// I/O functions like printf used for debug output
#include <stdio.h>
// string.h contains memcpy which we use to copy data between USB DPRAM and memory buffers
#include <string.h>
// use C wide characters (16 bit) for USB string descriptors
#include <wchar.h>
// required for initializing UART output
#include "pico/stdlib.h"
// USB data structures and registers
#include "hardware/structs/usb.h"
#include "hardware/regs/usb.h"
// for interrupt control register
#include "hardware/irq.h"
// for reset control
#include "hardware/resets.h"
// for set/clear register alternatives
#include "hardware/address_mapped.h"

// from button example, not sure whether they're all required.
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/sync.h"
#include "hardware/structs/ioqspi.h"
#include "hardware/structs/sio.h"

// USB descriptor definitions
#include "usb_defs.h"

#include "usb_char_map.h"


//#define USE_4BUTTONS_REPORT_TYPE

// unfortunately, wchar_t doesn't work properly.
// having const in the second place makes the strings constant
char const* stringDescriptors[] = {
	(const char[]){0x0409},	// first descriptor is the language code, English is used here. 16 Bit language ID defined by USB-IF
	"Golem.de",			// manufacturer
	"Cheatberry Pi Pico Macropad",	// product name :)
	"00000002"			// product serial number
};

// here we store the current PID value of endpoint 0. It's required when sending transactions containing more
// than one data packets. To simplify handling an array is used although we don't need to track EP 1 PID.
uint8_t epPIDNext[2];

// variable to remember device address as we must only change it after the Status phase of the Set Address transaction has completed.
uint usbDevAddr = 0;
// flag to remember whether we have already set the device address.
bool usbDevAddrSet = false;
// flag to remember whether host has configured the device
bool usbConfigured = false;

bool usbInitCompleted = false;

struct usb_device_descriptor devDesc = {
	.bLength		= sizeof(struct usb_device_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_DEVICE,
	.bcdUSB			= 0x0110,
	.bDeviceClass		= 0,// class and sub-class are 0 for HID devices in device descriptor.
	.bDeviceSubClass	= 0,// Class is defined in the interface descriptor.
	.bDeviceProtocol	= 0,
	.bMaxPacketSize0	= 64,
	.idVendor		= 0x0000,// I don't have a vendor ID...	
	.idProduct		= 0xb008,
	.bcdDevice		= 0,
	.iManufacturer		= 1,
	.iProduct		= 2,
	.iSerialNumber		= 3,
	.bNumConfigurations	= 1
};

struct usb_configuration_descriptor confDesc = {
	.bLength		= sizeof(struct usb_configuration_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_CONFIGURATION,
	.wTotalLength		= sizeof(struct usb_configuration_descriptor)
				+ sizeof(struct usb_interface_descriptor)//we only have one interface, otherwise this would have to be multiplied by the number of interfaces
				+ sizeof(struct usb_endpoint_descriptor)// same applies here, but we only have one endpoint (EP0 doesn't have a descriptor).
				+ 2 * sizeof(struct usb_hid_descriptor),
	.bNumInterfaces		= 1,
	.bConfigurationValue	= 1,
	.iConfiguration		= 0,
	.bmAttributes		= 0x80,// bus powered
	.bMaxPower		= 50// 100 mA should be more than the Pico actually needs
};

struct usb_interface_descriptor ifDesc = {
	.bLength		= sizeof(struct usb_interface_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_INTERFACE,
	.bInterfaceNumber	= 0,// zero-based!
	.bAlternateSetting	= 0,// TINA
	.bNumEndpoints		= 2,// we only use on interrupt IN endpoint
	.bInterfaceClass	= USB_CLASS_CODE_HID,// HID class 
	.bInterfaceSubClass	= 0,// no sub class, only relevant for keyboards and mice which can be boot interfaces as a hint to BIOS/UEFI
	.bInterfaceProtocol	= 0,// also just used by keyboards and mice to tell whether this is a keyboard or mouse
	.iInterface		= 0
};

struct usb_endpoint_descriptor ep1InDesc = {
	.bLength		= sizeof(struct usb_endpoint_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_ENDPOINT,
	.bEndpointAddress	= USB_DIRECTION_IN | 0x01,
	.bmAttributes		= USB_EP_TYPE_INTERRUPT,
	.wMaxPacketSize		= 0x08,// we have an 7-Byte-report (modifier byte, 6 keys); for interrupt endpoints, the host uses this value to reserve bus time
	.bInterval		= 0x0a// poll this endpoint every 10 ms
};

struct usb_endpoint_descriptor ep1OutDesc = {
	.bLength		= sizeof(struct usb_endpoint_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_ENDPOINT,
	.bEndpointAddress	= USB_DIRECTION_OUT | 0x01,
	.bmAttributes		= USB_EP_TYPE_INTERRUPT,
	.wMaxPacketSize		= 0x08,// we have an 7-Byte-report (modifier byte, 6 keys); for interrupt endpoints, the host uses this value to reserve bus time
	.bInterval		= 0x0a// poll this endpoint every 10 ms
};


// the report descriptor sequence of bytes; no report ID is used
// it can simply be copied from the HID spec (listing E.6)
const char reportDescriptor[] = {
	0x05, 0x01,// Usage Page(Generic Desktop)
	0x09, 0x06,// Usage(keyboard)
	0xa1, 0x01,// Collection(Application)
	0x05, 0x07,// Usage Page (Key Codes);
	// the following is kind of strange; although the modifier keys are reported using a bitmap,
	// they also have key codes assigned. With usage minimum and maximum the host is told which
	// key the bits map to. E.g., the LSBit maps to key code 224 (E0) -> LeftControl
	0x19, 0xE0,// Usage Minimum (224),
	0x29, 0xE7,// Usage Maximum (231),
	0x15, 0x00,// Logical Minimum (0),
	0x25, 0x01,// Logical Maximum (1),
	0x75, 0x01,// Report Size (1),
	0x95, 0x08,// Report Count (8),
	0x81, 0x02,// Input (Data, Variable, Absolute),
	// one reserved byte
	0x95, 0x01,// Report Count (1),
	0x75, 0x08,// Report Size (8),
	0x81, 0x01,// Input (Constant),
	// LED status output (shift, scroll and num lock)
	0x95, 0x05,// Report Count (5),
	0x75, 0x01,// Report Size (1),
	0x05, 0x08,// Usage Page (Page# for LEDs),
	0x19, 0x01,// Usage Minimum (1),
	0x29, 0x05,// Usage Maximum (5),
	0x91, 0x02,// Output (Data, Variable, Absolute),
	// 3 padding bits
	0x95, 0x01,// Report Count (1),
	0x75, 0x03,// Report Size (3),
	0x91, 0x01,// Output (Constant),
	// up to six keys
	0x95, 0x06,// Report Count (6),
	0x75, 0x08,// Report Size (8),
	0x15, 0x00,// Logical Minimum (0),
	0x25, 0x65,// Logical Maximum(101),
	0x05, 0x07,// Usage Page (Key Codes),
	0x19, 0x00,// Usage Minimum (0),
	0x29, 0x65,// Usage Maximum (101),
	0x81, 0x00,// Input (Data, Array),
	0xc0// End Collection
};


// the HID class descriptor precedes the report descriptor
struct usb_hid_descriptor hidDesc = {
	.bLength			= sizeof(struct usb_hid_descriptor),
	.bDescriptorType		= USB_DESC_TYPE_HID,
	.bcdHID				= 0x0111,// the latest HID spec is 1.11
	.bCountryCode			= 0,// device isn' localized; if it was, we could put a country ID here
	.bNumDescriptors		= 1,// just the report descriptor follows
	.bReportDescriptorType		= USB_DESC_TYPE_HID_REPORT,
	.wReportDescriptorLength	= sizeof(reportDescriptor)
};

void setupEndpoints() {
	uint bufferOff = (uint32_t)usb_dpram->epx_data ^ (uint32_t)usb_dpram;
	// configure endpoint 1, it uses offset 0 as endpoint 0 doesn't have a control register
	// most features are left unused, the IN endpoint is just enabled, set up as an interrupt endpoint
	// and the buffer address set to the beginning of the configurable buffer area in DPRAM (DPRAM + 0x180).
	// It's very important to set EP_CTRL_INTERRUPT_PER_BUFFER, otherwise we don't have a chance to clear
	// the done bit.
	usb_dpram->ep_ctrl[0].in = EP_CTRL_ENABLE_BITS | EP_CTRL_INTERRUPT_PER_BUFFER |
					(USB_BUFFER_TYPE_INT << EP_CTRL_BUFFER_TYPE_LSB) |
					bufferOff;
	
	printf("EP 1 IN: %08x\n");
	
	
	epPIDNext[0] = 1;
	epPIDNext[1] = 0;
}

// a handy function to trigger transmission of a buffer. Data is copied and all necessary flags are set for the controller to send it.
void usbTransmitBuffer(uint8_t ep, const char* buffer, uint16_t bytes, bool lastBuffer) {
	uint txCtrl = 0;
	uint dstAddr;

	// do some boundary checks first
	if(ep > 2) {
		printf("endpoint %u isn't active, can't send\n", ep);
	} else if(bytes > 64) {
		printf("sorry, I can't send more than 64 bytes; %d requested\n", bytes);
	} else {
		dstAddr = usb_dpram->ep0_buf_a + (ep << 7);
		// we only use buffer 0 for simplicity
		if((buffer != NULL) && (bytes > 0))
			memcpy(dstAddr, buffer, bytes);
		
		txCtrl = USB_BUF_CTRL_AVAIL | USB_BUF_CTRL_FULL | ((epPIDNext[ep]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID) | ((lastBuffer) ? USB_BUF_CTRL_LAST : 0) | bytes;
		
		// NOTE: the USB controller's registers are mapped via usb_hw while buffer control resides in DPRAM!
		usb_dpram->ep_buf_ctrl[ep].in = txCtrl;
		
		// toggle PID
		epPIDNext[ep] ^= 1;
	}
	
//	printf("-0x%08x data -> %08x\n", txCtrl, dstAddr);
}

// an even more handy function which copies data from several sources into the transmit buffer
void usbTransmitBuffer_gather(uint8_t ep, uint nSources, const char** buffer, uint16_t* bytes, bool lastBuffer) {
	uint txCtrl = 0, totalBytes, i, off;
	
	for(i = 0, totalBytes = 0; i < nSources; ++i)
		totalBytes += bytes[i];

	// do some boundary checks first
	if(ep > 2) {
		printf("endpoint %u isn't active, can't send\n", ep);
	} else if(totalBytes > 64) {
		printf("sorry, I can't send more than 64 bytes; %d requested\n", totalBytes);
	} else {
		for(i = 0, off = 0; i < nSources; ++i) {
			// we only use buffer 0 for simplicity
			if((buffer != NULL) && (bytes[i] > 0))
				memcpy(usb_dpram->ep0_buf_a + (ep << 7) + off, buffer[i], bytes[i]);

			off += bytes[i];
		}
		
		txCtrl = USB_BUF_CTRL_AVAIL | USB_BUF_CTRL_FULL | ((epPIDNext[ep]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID) | ((lastBuffer) ? USB_BUF_CTRL_LAST : 0) | totalBytes;
		
		// NOTE: the USB controller's registers are mapped via usb_hw while buffer control resides in DPRAM!
		usb_dpram->ep_buf_ctrl[ep].in = txCtrl;
		
		// toggle PID
		epPIDNext[ep] ^= 1; 
	}
	
	printf("0x%08x\n", txCtrl);
}

// function to handle setup transactions (which start control transfers)
void handleSetupTransaction() {
	volatile struct usb_request *req = (volatile struct usb_request *) &usb_dpram->setup_packet;
	uint bRequest, bmRequestType, wLength, wValue;
	bRequest = req->bRequest;
	bmRequestType = req->bmRequestType;
	wLength = req->wLength;
	wValue = req->wValue;
	
	epPIDNext[0] = 1;// Setup requests always use PID 0, the first data packet (or status packet) has to use PID 1.
	
	// first check direction of data transfer; for OUT there's less to do
	if(USB_REQ_GET_DIR(req) == USB_DIRECTION_OUT) {
		bool doStatus = false;
		
		switch(bRequest) {
		case USB_REQ_SET_ADDRESS:
			// buffer the address; it is written to the USB controller's register after the Status stage
			// completes. That's the case when we receive the next buffer status interrupt.
			// The address the host assigned us is in the lower wValue
			usbDevAddr = wValue & 0x7f;
			
			// check whether the address is okay
			if(usbDevAddr > 127) {
				// no, it's not, something is wrong.
				usbDevAddr = 0;
			}
			
			printf("got address %d\n", usbDevAddr);
			
			doStatus = true;
			break;
		case USB_REQ_SET_CONFIGURATION:
			// as we only have one configuration there's not much to do here.
			usbConfigured = true;
			doStatus = true;
			printf("configuration set.\n");
			break;
		}
		
		// Both transactions have no data phase, but to complete the transaction we have to send
		// a 0 Byte packet in the Status stage. This is done by putting 0 bytes of data into EP0 IN.
		if(doStatus)
			usbTransmitBuffer(0, NULL, 0, true);
		
		printf("OUT request, request: %02x type: 0x%02x\n", bRequest, bmRequestType);
	} else {
		char *sources[5];
		uint16_t bytes[5];
		
		// for IN requests, the host sends an empty OUT request in the Status stage.
		if(bRequest == USB_REQ_GET_DESCRIPTOR) {
			switch(USB_REQ_GET_DESC_TYPE(req)) {
			case USB_DESC_TYPE_DEVICE:
				// send the device descriptor to host
				usbTransmitBuffer(0, (char*)&devDesc, devDesc.bLength, true);
				printf("Device Descriptor sent\n");
				break;
			case USB_DESC_TYPE_CONFIGURATION:
				if(req->wLength == confDesc.bLength) {
					// send only configuration descriptor
					usbTransmitBuffer(0, (char*)&confDesc, confDesc.bLength, true);
				} else {
					// when the configuration descriptor is requested we must return, in this order:
					// configuration descriptor, interface descriptor, HID descriptor and endpoint descriptor.
					sources[0] = (char*)&confDesc;
					sources[1] = (char*)&ifDesc;
					sources[2] = (char*)&hidDesc;
					sources[3] = (char*)&ep1InDesc;
					sources[4] = (char*)&ep1OutDesc;
					bytes[0] = confDesc.bLength;
					bytes[1] = ifDesc.bLength;
					bytes[2] = hidDesc.bLength;
					bytes[3] = ep1InDesc.bLength;
					bytes[4] = ep1OutDesc.bLength;
					
					usbTransmitBuffer_gather(0, 5, sources, bytes, true);
				}
				printf("Configuration Descriptor sent\n");
				break;
			case USB_DESC_TYPE_STRING:
				// string descriptors are composed from the generic descriptor and the respective string.
				char buf[64];
				struct usb_descriptor_common *sd = (struct usb_descriptor_common*)buf;
				uint sid = USB_REQ_GET_DESC_INDEX(req);
				
				if(sid == 0) {
					buf[2] = stringDescriptors[0][0];
					buf[3] = stringDescriptors[0][1];
					
					sd->bLength = 4;
					sd->bDescriptorType = USB_DESC_TYPE_STRING;
				} else {
					uint strBytes = strlen(stringDescriptors[sid]);
					
					sd->bLength = (strBytes << 1) + 2;
					sd->bDescriptorType = USB_DESC_TYPE_STRING;
					
					for(uint i = 0; i < strBytes; ++i) {
						buf[(i << 1) + 2] = stringDescriptors[sid][i];
						buf[(i << 1) + 3] = 0;
					}
				}
				
				usbTransmitBuffer(0, buf, sd->bLength, true);
				printf("String Descriptor sent\n");
				break;
			case USB_DESC_TYPE_HID:
				usbTransmitBuffer(0, (const char*)&hidDesc, (req->wLength < hidDesc.bLength) ? req->wLength : hidDesc.bLength, true);
				break;
			case USB_DESC_TYPE_HID_REPORT:
				usbTransmitBuffer(0, (const char*)reportDescriptor, sizeof(reportDescriptor), true);
				usbInitCompleted = true;
				break;
			}
		}
		
		printf("IN request, code: %02x type: 0x%x wValue: %d wLength: %d\n", bRequest, bmRequestType, wValue, wLength);
	}
}

// interrupt handler for USB interrupt; we check what caused the interrupt and do the appropriate thing.
void usbIntHandler() {
	// USB interrupt handler
	uint32_t status = usb_hw->ints;
	uint32_t handled = 0;
	
	//printf("int");

	// first check what caused the interrupt. There may be several causes.
	// Setup packet received, starting a control
	if(status & USB_INTS_SETUP_REQ_BITS) {
		handled |= USB_INTS_SETUP_REQ_BITS;
		// hw_clear_alias clears bits in registers atomically to avoid race conditions between the two ARM cores.
		// This isn't an issue here as we only use one core, but good to know. There are similar functions for set and xor
		// as well as XIP cache.
		hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_SETUP_REC_BITS;
		handleSetupTransaction();
	}

	// buffer status interrupt is triggered once a buffer completes.
	if (status & USB_INTS_BUFF_STATUS_BITS) {
		uint bufferBits, buffersClearMask = 0;
		handled |= USB_INTS_BUFF_STATUS_BITS;
		
		bufferBits = usb_hw->buf_status;
		
		// check which endpoint's status changed... it can only be 0 or 1.
		if(bufferBits & 0x01) {
			// endpoint 0 IN
			buffersClearMask |= 0x01;
			
			// Set Address ends with a Status phase which is a 0 byte IN packet. Once that's done, assign the new address.
			if((!usbDevAddrSet) && (usbDevAddr != 0)) {
				// Status stage complete, set new address in control register
				usb_hw->dev_addr_ctrl = usbDevAddr;
				usbDevAddrSet = true;
				
				printf("address set in controller\n");
			} else {
				usb_dpram->ep_buf_ctrl[0].out = USB_BUF_CTRL_AVAIL | ((epPIDNext[0]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID);
				
				epPIDNext[0] ^= 1;
			}
		}
		if(bufferBits & 0x02) {
			// endpoint 0 OUT, we'll only see this for Set Address and Set Configuration.
			buffersClearMask |= 0x02;
			
			// make buffer available for USB controller again
			usb_dpram->ep_buf_ctrl[0].out = USB_BUF_CTRL_AVAIL;
		}
		if(bufferBits & 0x04) {
			// endpoint 1 IN, that's where we put the reports.
			buffersClearMask |= 0x04;
		}
		
		// NEWNEWNEW: we need to handle endpoint 1 OUT as we told the host we have status LEDs (which we haven't, but the BIOS demands it)
		if(bufferBits & 0x08) {
			// we don't actually use that data since we only have one LED, so just discard it.
			buffersClearMask |= 0x08;
		}
		
		// now clear all flags we handled. Every bit that's 1 in buffersClearMask is atomically cleared to 0 in the register.
		hw_clear_alias(usb_hw)->buf_status = buffersClearMask;
		
//		printf("ep %08x buffers done\n", buffersClearMask);
	}

	// a bus reset, we have to reset the device to unconfigured state and revert address to 0
	if(status & USB_INTS_BUS_RESET_BITS) {
		printf("BUS RESET\n");
		handled |= USB_INTS_BUS_RESET_BITS;
		hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_BUS_RESET_BITS;

		usbDevAddr = 0;
		usbDevAddrSet = false;
		hw_clear_alias(usb_hw)->dev_addr_ctrl = 0;
		usbConfigured = false;
	}

	if (status ^ handled) {
		printf("unhandled interrupt source from USB controller - how could this happen?\n");
	}
}

// configure the USB block.
void setupUsbDevice() {
	// reset the USB controller first and wait until this is done
	reset_block(RESETS_RESET_USBCTRL_BITS);
	unreset_block_wait(RESETS_RESET_USBCTRL_BITS);

	// clear the USB block's memory
	memset(usb_dpram, 0, sizeof(*usb_dpram));

	// enable the USB interrupt in the interrupt controller, which can be raised by several events
	// which we'll enable in the USB controller's interrupt enable register below.
	// Also set the handler function.
	irq_set_exclusive_handler(USBCTRL_IRQ, usbIntHandler);
	irq_set_enabled(USBCTRL_IRQ, true);

	// Mux the controller to the onboard usb phy
	usb_hw->muxing = USB_USB_MUXING_TO_PHY_BITS;// | USB_USB_MUXING_SOFTCON_BITS;

	// Force VBUS detect so the device thinks it is plugged into a host
	usb_hw->pwr = USB_USB_PWR_VBUS_DETECT_BITS | USB_USB_PWR_VBUS_DETECT_OVERRIDE_EN_BITS;

	// enable USB controller in device mode (device mode is implicit)
	usb_hw->main_ctrl = USB_MAIN_CTRL_CONTROLLER_EN_BITS;

	// now we tell the controller when to raise an interrupt:
	// when a buffer is completed (buff_status), when it detects a bus reset (which is part of device enumeration),
	// when a Setup packet is received which starts data transmission. The other sources aren't relevant.
	// Actually, we only need the status bit to know when it's okay to change the device address. But since all data we transmit
	// is less than 64 bytes and therefore we don't need to know when a buffer is finished since we're not using double buffering.
	// If we did, we would know that we can fill the next buffer once we receive the status interrupt.
	usb_hw->inte = USB_INTE_BUFF_STATUS_BITS |
			USB_INTE_BUS_RESET_BITS |
			USB_INTE_SETUP_REQ_BITS;

	// setup the endpoint control register(s)
	setupEndpoints();
	
	// every ep0 buffer completed sets BUFF_STATUS and hence generates an interrupt,
	// enable pull-up resistor to tell host this is a full speed device
	usb_hw->sie_ctrl = USB_SIE_CTRL_EP0_INT_1BUF_BITS | USB_SIE_CTRL_PULLUP_EN_BITS;
}

// reading the pico's button is a bit tricky, this is taken from the example.
bool __no_inline_not_in_flash_func(get_bootsel_button)() {
    const uint CS_PIN_INDEX = 1;

    // Must disable interrupts, as interrupt handlers may be in flash, and we
    // are about to temporarily disable flash access!
    uint32_t flags = save_and_disable_interrupts();

    // Set chip select to Hi-Z
    hw_write_masked(&ioqspi_hw->io[CS_PIN_INDEX].ctrl,
                    GPIO_OVERRIDE_LOW << IO_QSPI_GPIO_QSPI_SS_CTRL_OEOVER_LSB,
                    IO_QSPI_GPIO_QSPI_SS_CTRL_OEOVER_BITS);

    // Note we can't call into any sleep functions in flash right now
    for (volatile int i = 0; i < 1000; ++i);

    // The HI GPIO registers in SIO can observe and control the 6 QSPI pins.
    // Note the button pulls the pin *low* when pressed.
    bool button_state = !(sio_hw->gpio_hi_in & (1u << CS_PIN_INDEX));

    // Need to restore the state of chip select, else we are going to have a
    // bad time when we return to code in flash!
    hw_write_masked(&ioqspi_hw->io[CS_PIN_INDEX].ctrl,
                    GPIO_OVERRIDE_NORMAL << IO_QSPI_GPIO_QSPI_SS_CTRL_OEOVER_LSB,
                    IO_QSPI_GPIO_QSPI_SS_CTRL_OEOVER_BITS);

    restore_interrupts(flags);

    return button_state;
}

//char *kbString = "Hallo, liebe Golem-Leser!\n";
char *kbString = "8lz9lz";

int main() {
	uint8_t report[8];
	int kbStrLen = strlen(kbString);
	int kbStrOff = 0;
	uint16_t txData;
	char lastChar = 0;
	bool done = false;
	int tSleep;

#ifdef PICO_DEFAULT_LED_PIN
	gpio_init(PICO_DEFAULT_LED_PIN);
	gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
#endif
	
	stdio_init_all();
	printf("USB macropad started.\nReport descriptor size: %d\n", hidDesc.wReportDescriptorLength);
	setupUsbDevice();
	printf("init done, starting data generation loop.\n");
	
	while(!usbInitCompleted) {
#ifdef PICO_DEFAULT_LED_PIN
		// blink the LED to indicate configuration status: off -> not enumerated, blinking -> address assigned, on: configured
		if(!usbDevAddrSet) {
			gpio_put(PICO_DEFAULT_LED_PIN, 0);
		} else {
			if(!usbConfigured) {
				gpio_put(PICO_DEFAULT_LED_PIN, !gpio_get(PICO_DEFAULT_LED_PIN));
			} else {
				gpio_put(PICO_DEFAULT_LED_PIN, 1);
			}
		}
#endif
		sleep_ms(100);
	}
	
	report[1] = 0;
	report[3] = 0;
	report[4] = 0;
	report[5] = 0;
	report[6] = 0;
	report[7] = 0;
	
	gpio_put(PICO_DEFAULT_LED_PIN, 0);
	
	while(true) {
		done = false;
		
		while(!get_bootsel_button()) {
			sleep_ms(100);
			gpio_put(PICO_DEFAULT_LED_PIN, 1);
			sleep_ms(100);
			gpio_put(PICO_DEFAULT_LED_PIN, 0);
		}
		
//		printf("start keyboard loop\n");
		
		while(!done) {
			kbStrOff = 0;
			lastChar = 0;
			
			gpio_put(PICO_DEFAULT_LED_PIN, 1);
			
			while (kbStrOff < kbStrLen) {
				sleep_ms(50);
				
				// now generate the report packet;
				// every packet contains one key stroke, in case a letter is repeated, we need to send an empty
				// packet first to "release the key".
				if(kbString[kbStrOff] != lastChar) {
					lastChar = kbString[kbStrOff];
					txData = charMapping[kbString[kbStrOff++] & 0x7f];
					report[0] = (uint8_t)(txData >> 8);
					report[2] = (uint8_t)(txData & 0xff);
				} else {
					lastChar = 0;
					report[0] = 0;
					report[2] = 0;
				}	

				// now transfer the report to the endpoint 1 buffer.
				usbTransmitBuffer(1, (const char*)report, 8, true);
			}
			
			gpio_put(PICO_DEFAULT_LED_PIN, 0);
			
			// clear all keys before we wait for the next round.
			sleep_ms(20);
			report[0] = 0;
			report[2] = 0;
			usbTransmitBuffer(1, (const char*)report, 8, true);
			
			// now wait 25 seconds, but sleep in small intervals so we notice a button press.
			for(tSleep = 0; (tSleep < 50) && !done; tSleep++) {
				sleep_ms(500);
				done = get_bootsel_button();
			}
		}
		
//		printf("keyboard loop terminated.\n");
		
		// wait a bit so we have a chance to release the button.
		sleep_ms(2000);
	}
	
	return 0;
}
