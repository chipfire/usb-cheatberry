#ifndef USB_CHAR_MAP_H__
#define USB_CHAR_MAP_H__

// This file provides a mapping from ASCII codes to character scan codes.
// Note: In order to transmit uppercase characters, we have to set the modifier byte.
// Its description can be found in the HID documentation, section 8.3
// It tells the host whether one onf the shift, control, alt or GUI keys is pressed.
// So for every character we have to set it accordingly, i.e., uppercase characters
// require one of the shift keys to be pressed while, e.g., @ would require the right
// alt key to be pressed.
// Modifier Byte is defined like this:
// Bit 0: left ctrl
// Bit 1: left shift
// Bit 2: left alt
// Bit 3: left gui
// Bit 4: right ctrl
// Bit 5: right shift
// Bit 6: right alt
// Bit 7: right gui

// Some characters are used to represent control codes while others encode keys like DEL.

// for these codes, the next two bytes are treated as an argument, i.e., the jump offset
// or number of cycles to delay.
const uint8_t CMD_JUMP		= 0;
const uint8_t CMD_DELAY		= 1;

// special keys in range 2 .. 31 (used a bit different than in ASCII)
const uint8_t KEY_DEL		= 2;
const uint8_t KEY_BACKSPACE	= 3;
const uint8_t KEY_ESC		= 4;
const uint8_t KEY_TAB		= 5;
const uint8_t KEY_ENTER		= 6;
const uint8_t KEY_INSERT	= 7;
const uint8_t KEY_PRINT		= 8;
const uint8_t KEY_HOME		= 9;
const uint8_t KEY_END		= 10;
const uint8_t KEY_PAGE_UP	= 11;
const uint8_t KEY_PAGE_DOWN	= 12;
// 16 .. 31
const uint8_t KEY_F1		= 16;
const uint8_t KEY_F2		= 17;
const uint8_t KEY_F3		= 18;
const uint8_t KEY_F4		= 19;
const uint8_t KEY_F5		= 20;
const uint8_t KEY_F6		= 21;
const uint8_t KEY_F7		= 22;
const uint8_t KEY_F8		= 23;
const uint8_t KEY_F9		= 24;
const uint8_t KEY_F10		= 25;
const uint8_t KEY_F11		= 26;
const uint8_t KEY_F12		= 27;
const uint8_t KEY_LEFT		= 28;
const uint8_t KEY_RIGHT		= 29;
const uint8_t KEY_UP		= 30;
const uint8_t KEY_DOWN		= 31;

// these are always a combination of modifier byte (MSB) and the respective key code (LSB)
uint16_t charMapping[128] = {
	0x0000,// jump command
	0x0000,// delay command
	0x004c,
	0x002a,
	0x0029,
	0x002b,
	0x0028,
	0x0049,
	0x0046,
	0x004a,
	0x004d,
	0x004b,
	0x004e,
	0x0000,
	0x0000,
	0x0000,
	// 16
	0x003a,
	0x003b,
	0x003c,
	0x003d,
	0x003e,
	0x003f,
	0x0040,
	0x0041,
	0x0042,
	0x0043,
	0x0044,
	0x0045,
	0x0050,
	0x004f,
	0x0052,
	0x0051,
	// 32
	0x002c,// space
	0x021e,// !
	0x021f,// "
	0x0032,// # - according to AT-101 ke code, this should be the correct code (as US-keyboards lack this key)
	0x0221,// $
	0x0222,// %
	0x0223,// &
	0x002e,// ´
	0x0225,// (
	0x0226,// )
	0x0230,// *
	0x0030,// +
	0x0036,// ,
	0x0038,// -
	0x0037,// .
	0x0224,// /
	// 48
	0x0027,// 0
	0x001e,
	0x001f,
	0x0020,
	0x0021,
	0x0022,
	0x0023,
	0x0024,
	0x0025,
	0x0026,// 9
	0x0237,// :
	0x0236,// ;
	0x0064,// <
	0x0227,// =
	0x0264,// >
	0x022d,// ?
	// 64
	0x4014,// @
	0x0204,// A
	0x0205,
	0x0206,
	0x0207,
	0x0208,
	0x0209,
	0x020a,
	0x020b,
	0x020c,
	0x020d,
	0x020e,
	0x020f,
	0x0210,
	0x0211,
	0x0212,
	// 80
	0x0213,
	0x0214,
	0x0215,
	0x0216,
	0x0217,
	0x0218,
	0x0219,
	0x021a,
	0x021b,
	0x021d, // Y
	0x021c, // Z - key codes are swapped as we're asked to report according to US layout
	0x4025,// [
	0x402d,// backslash
	0x4026,// ]
	0x0035,// ^
	0x0238,// _
	// 96
	0x022e,// ` (Shift + =)
	0x0004,// a
	0x0005,
	0x0006,
	0x0007,
	0x0008,
	0x0009,
	0x000a,
	0x000b,
	0x000c,
	0x000d,
	0x000e,
	0x000f,
	0x0010,
	0x0011,
	0x0012,
	// 112
	0x0013,
	0x0014,
	0x0015,
	0x0016,
	0x0017,
	0x0018,
	0x0019,
	0x001a,
	0x001b,
	0x001d,
	0x001c,// z, again swapped with y
	0x4024,// {
	0x4064,// | - Non-US \ and | (identified this by AT-101 key code)
	0x4027,// }
	0x4030,// ~
	0x004c// this actually is DEL
};

#endif
