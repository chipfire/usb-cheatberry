# usb-cheatberry

A custom macropad made to generate keyboard controls for games. It behaves like a USB keyboard and sends keystroke events to the host.
Example builds Zerglings in two hatcheries in original StarCraft.